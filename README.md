## Teste API Digito Único - Tiago Oliveira Castro

Este projeto implementa uma API para o cadastro de usuários e cálculo do digito único. Abaixo serão colocadas algumas informações importantes referentes a execução do projeto, decisões de implementação realizadas, etc.

---

## Execução do Código

Para executar o código, dentro da pasta **test/**, utilize o seguinte comando:

                                              mvn clean install
Em seguida, dentro da pasta **test/target**, execute:

                                            java -jar restprj-1.jar 

Isso irá fazer com que o código rode no localhost da máquina, assim, a API estará disponível em: **localhost:8080/rest/**


---

## Usuários
Os usuários possuem 3 informações principais: identificador, nome e email. 

Um usuário é criado por meio de uma requisição POST para **/rest/users/create**, sendo enviados dois parâmetros: nome e email. Feito isso, é retornado um identificador (id) que pode ser utilizado para realizar outras requisições na API.

---


## Digito Único

Para o cálculo do digito único foi utilizado foi utilizada uma solução recursiva. Apesar desse tipo de solução implicar em possível maior gasto de memória, ela foi escolhida devido a simplicidade e clareza de código. Isso permite mais fácil manutenção e leitura.

O cáculo do digito único pode ser feito por dois endpoints:
1 - **/rest/singleDigit GET** realiza o cálculo sem associação de um usuário.
2 - **/rest/singleDigit POST** realiza o cálculo associado a um usuário, salvando o resultado no banco.


---

## Banco de Dados

O banco de dados utilizado foi o SQLite. Como pedido no enunciado, foi utilizada a sua versão em memória. Devido a este fato, foi utilizada somente uma conexão com o banco dados. Para uma solução que fosse para produção, com um banco de dados não em memória. poderia ser facilmente adicionada uma pool de conexões.

As informações no banco foram divididas em duas tabelas, uma para os **usuários** e outra para os **resultados**. Sendo que cada resultado está associado a algum usuário.

Existem duas classes principais para acessar dados do banco, UsersDataAccess e ResultsDataAccess, que são uma interface para acessar dados respectivamente das tabelas de usuários e resultados.

---

## Criptografia

Para implementar a criptografia, foi utilizada como base o seguinte tutorial: https://www.devglan.com/java8/rsa-encryption-decryption-java

É importante lembrar que a Chave Pública fornecida deve ser no formato Base 64, podendo ser gerada, por exemplo,
no seguinte link: https://www.devglan.com/java8/rsa-encryption-decryption-java

O endpoint **/rest/publicKey/{id}** é utilizado para armazenar a chave pública do usuário.

O endpoint **/rest/encrypt** é utilizado para encriptar os dados do usuário cujo id encontrasse no corpo da requisição POST.

O endpoint **/rest/decrypt** é utilizado para decriptar os dados do usuário passado no corpo da requisição POST. Além do usuário também é necessário passar a chave privada.

---

## API

A documentação seguindo o formato open API foi gerada e está dentro da pasta test. Mesmo assim, abaixo tem-se uma descrição geral de cada endpoint.

    POST /rest/users/create - Envia nome e email. Recebe um identificador.
  
    GET /rest//users/read/{id} - Envia um identificador. Recebe as informações do usuário: nome, email e identificador.

    PUT /users/update/{id} - Envia um identificador, nome e email. Atualiza nome e email do usuário.
  
    DELETE /rest/users/delete/{id} - Envia um identificador. Deleta informações do usuário.

    POST /rest/singleDigit - Envia parâmetros identificador, n e k. O resultado do digito único é salvo no banco.
  
    GET /rest/singleDigit?number=123&k=4 - Envia parâmetros number e k. Recebe o resultado do digito único.

    GET /rest/results/{id} - Envia um identificador. Recebe todos resultados calculados para este usuário.

    POST /rest/publicKey - Envia um identificador e uma chave pública. Ela é salva no banco, sendo associada ao usuário.

    POST /rest/encrypt - Envia um identificador. As informações de nome e email são criptografadas com a chave pública associada.
    
    POST /rest/decrypt - Envia um identificador. Desencripta as informações do usuário.

---

## Testes de Unidade e Integração

Foram criados testes de unidade para as classes que trazem a lógica de Cache e também a lógica do cálculo do digito único.

Foram gerados testes de integração utilizando o Postman. Estes testes utilizam a grande maioria dos endpoints, testando a integração com o banco de dadados, etc. Antes de rodar os testes com o Postman, execute os comandos "mvn clean install" e "java -jar restprj-1.jar".
