/** 
 * DatabaseConnection is a class responsible for managing database 
 * connections. This class is a singleton.
 * 
 * @author Tiiago Oliveira Castro
 * @version 1.0
 * @since 2019-11-24
 */


package com.rest.test;

import java.sql.*;
import org.sqlite.*;

import java.util.List;
import java.util.ArrayList;

import com.rest.data.User;

public class DatabaseConnection {

	private static DatabaseConnection instance = null;
    private static Connection connection = null;

    /**
     * Private constructor because DatabaseConnection is a singleton.
     */
	private DatabaseConnection() {

	}

    /**
     * Method that initialize the instance of DatabaseConnection.
     * @return Instance of DatabaseConnection.
     */
    public static DatabaseConnection getInstance(){
        if (instance == null) {
            instance = new DatabaseConnection();
        }
        return instance;
    }

    /**
     * Method that returns a connection.
     * @return a connection to the in memory database.
     */
    public static Connection getConnection() {

        if(connection == null){
            try{ 
                Class.forName("org.sqlite.JDBC");
        	    connection = DriverManager.getConnection("jdbc:sqlite::memory:?cache=shared");

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        return connection;
    }
}
