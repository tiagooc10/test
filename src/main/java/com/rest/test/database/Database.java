package com.rest.test;

/** 
 * Database is the class responsible for initializing the database.
 * 
 * @author Tiiago Oliveira Castro
 * @version 1.0
 * @since 2019-11-24
 */

import java.sql.*;
import org.sqlite.*;

import java.util.List;
import java.util.ArrayList;

import com.rest.data.User;


public class Database {

	public Database() {
	
    }

    /**
     * [initializeTables description]
     * @throws ClassNotFoundException [description]
     */
    public void initializeTables() throws ClassNotFoundException{

        Class.forName("org.sqlite.JDBC");

        try{ 

            DatabaseConnection instance = DatabaseConnection.getInstance();
            Connection connection = instance.getConnection();

            // SQL statement for creating a new table
            String sql = "CREATE TABLE users (\n"
                    + "    id_user integer PRIMARY KEY,\n"     
                    + "    name text NOT NULL,\n"
                    + "    email text NOT NULL,\n"
                    + "    public_key text\n"
                    + ");";


            String sql2 = "CREATE TABLE results (\n"
                    + "    id_result integer PRIMARY KEY,\n"    
                    + "    id_user integer NOT NULL,\n"                 
                    + "    n text NOT NULL,\n"
                    + "    k integer NOT NULL,\n"
                    + "    result integer NOT NULL\n"
                    + ");";
            
            try{
                Statement stmt = connection.createStatement();
                stmt.execute(sql);
                stmt.execute(sql2);

            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}