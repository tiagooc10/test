/** 
 * UserDataAccess is a class responsible for communicating and interacting 
 * with the database for user-related informations.
 * 
 * @author Tiiago Oliveira Castro
 * @version 1.0
 * @since 2019-11-24
 */


package com.rest.test;

import java.sql.*;
import org.sqlite.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Base64;

import com.rest.data.User;
import com.rest.data.FullResult;
import com.rest.data.Result;

public class UserDataAccess {

    /**
     * Method that inserts an user in the database.
     * @param  name
     * @param  email
     * @return the user id.
     */
    public Integer createUser(String name, String email) {

        Integer id = null;
        Connection connection = null;
 
        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "INSERT INTO users(name,email) VALUES(?,?)";
            PreparedStatement pstmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();

            ResultSet rs = pstmt.getGeneratedKeys();
            if(rs.next()){
                id = rs.getInt(1);
            }

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return id;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return id;
        }

        return id;
    }

    /**
     * Method that reads an user information from the database.
     * @param  id_user
     * @return user information: name, email and id.
     */
    public User readUser(int id_user){

        User user = new User();
        Connection connection = null;
        
        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "SELECT id_user, name, email FROM users WHERE id_user=" + id_user;
            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);
            
            // loop through the result set
            while (rs.next()) {
                user.setName(rs.getString("name"));
                user.setEmail(rs.getString("email"));
                user.setId(rs.getInt("id_user"));

            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return user;
    }

    /**
     * Method that reads an user information from the database.
     * @param email
     * @return user information: name, email and id.
     */
    public User readsUser(String email){

        User user = new User();
        
        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            Connection connection = instance.getConnection();

            String sql = "SELECT name, email, id FROM users WHERE email = '" + email + "'";
            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);
            
            // loop through the result set
            while (rs.next()) {
                user.setName(rs.getString("name"));
                user.setEmail(rs.getString("email"));
                user.setId(rs.getInt("id"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 

        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return user;
    }

    /**
     * Method that updates an user information from the database.
     * @param  name
     * @param  email
     * @return boolean that is True if the user was updated.
     */
    public boolean updateUser(int id, String name, String email) {

        Connection connection = null;

        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "UPDATE users SET name=?, email=? WHERE id_user=" + id;
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, name);
            pstmt.setString(2, email);
            pstmt.executeUpdate();
        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return true;
    }

    /**
     * Method that deletes an user information from the database.
     * @param  name
     * @param  email
     * @return boolean that indicates if the user was deleted.
     */
    public boolean deleteUser(int id_user) {

        Connection connection = null;
 
        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "DELETE FROM users WHERE id_user =" + id_user;

            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.executeUpdate();
 
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Print all users in the database.
     * @return
     */
    public List<Integer> selectAllUsers(){

        List<Integer> results = new ArrayList<>();
        Connection connection = null;
        
        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "SELECT * FROM users";
            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);
            
            // loop through the result set
            while (rs.next()) {
                System.out.println("id:" + rs.getInt("id_user"));
                System.out.println("name:" + rs.getString("name"));
                System.out.println("email:" + rs.getString("email"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return results;
    }


    /**
     * Method that update user information including a public key.
     * @param  id_user
     * @param  public_key
     * @return boolean that is True if the public key was inserted.
     */
    public boolean updatePublicKey(int id_user, String public_key) {

        Connection connection = null;

        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "UPDATE users SET public_key=? WHERE id_user=" + id_user;
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, public_key);
            pstmt.executeUpdate();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return true;
    }

    /**
     * Method that selects user public key.
     * @param  id_user
     * @return String public key from the users table.
     */
    public String selectPublicKey(int id_user){

        String public_key = null;
        Connection connection = null;
        
        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "SELECT public_key FROM users WHERE id_user=" + id_user;
            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);
            
            // loop through the result set
            while (rs.next()) {
                public_key = rs.getString("public_key");
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return public_key;
    }

    /**
     * Method that encrypt information of a given user using a given Public Key.
     * @param  id_user
     * @param  public_key
     * @return boolean that indicates if the data was encrypted.
     */
    public boolean encryptUserData(int id_user, String public_key) {

        Cryptography c = new Cryptography();
        
        User user = new User();
        user = readUser(id_user);

        String email = user.getEmail();
        String name = user.getName();

        Connection connection = null;

        try{
            String encryptedName = Base64.getEncoder().encodeToString(c.encrypt(name, public_key));
            String encryptedEmail = Base64.getEncoder().encodeToString(c.encrypt(email, public_key));

            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "UPDATE users SET name=?, email=? WHERE id_user=" + id_user;
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, encryptedName);
            pstmt.setString(2, encryptedEmail);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Method that decrypt information of a given user using a given Private Key.
     * @param  id_user
     * @param  public_key
     * @return boolean that indicates if the data was decrypted.
     */
    public boolean decryptUserData(int id_user, String private_key) {

        Cryptography c = new Cryptography();
        
        User user = new User();
        user = readUser(id_user);

        String name = user.getName();
        String email = user.getEmail();

        Connection connection = null;

        try{
            String decryptedName = c.decrypt(name, private_key);
            String decryptedEmail = c.decrypt(email, private_key);

            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "UPDATE users SET name=?, email=? WHERE id_user=" + id_user;
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setString(1, decryptedName);
            pstmt.setString(2, decryptedEmail);
            pstmt.executeUpdate();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return true;
    }

}