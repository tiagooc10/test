/** 
 * ResultsDataAcess is a class responsible for communicating and interacting 
 * with the database for results-related informations.
 * 
 * @author Tiiago Oliveira Castro
 * @version 1.0
 * @since 2019-11-24
 */


package com.rest.test;

import java.sql.*;
import org.sqlite.*;

import java.util.List;
import java.util.ArrayList;
import java.util.Base64;

import com.rest.data.User;
import com.rest.data.FullResult;
import com.rest.data.Result;

import com.rest.test.UserDataAccess;

public class ResultsDataAccess {

    /**
     * Method that select all results from a given user using his email.
     * @param  id_user Id of the user.
     * @return         List with all results from a given user.
     */
    public List<FullResult> selectAllResults(int id_user){

        List<FullResult> results = new ArrayList<>();
        Connection connection = null;
        
        try{
            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "SELECT * FROM results WHERE id_user=" + id_user;
            Statement stmt  = connection.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                FullResult full_result = new FullResult();
                full_result.setResult(rs.getInt("result"));
                full_result.setN(rs.getString("n"));
                full_result.setK(rs.getInt("k"));

                results.add(full_result);
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return results;
    }

    /**
     * Method that insert a result in the database.
     * @param  user  
     * @param  n     
     * @param  k  
     * @param  result
     * @return boolean that is true if the result was inserted.
     */
    public boolean insertResult(int user, String n, int k, int result) {
 
        Connection connection = null;

        try {

            DatabaseConnection instance = DatabaseConnection.getInstance();
            connection = instance.getConnection();

            String sql = "INSERT INTO results(id_user,n,k,result) VALUES(?,?,?,?)";
            PreparedStatement pstmt = connection.prepareStatement(sql);
            pstmt.setInt(1, user);
            pstmt.setString(2, n);
            pstmt.setInt(3, k);
            pstmt.setInt(4, result);
            pstmt.executeUpdate();

        } catch (SQLException e) {

            System.out.println(e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return true;
    }

}