/** 
 * Cache is a class that implements a simple cache system, using arrays to store 10
 * single digit calculations in memory.
 * 
 * @author Tiiago Oliveira Castro
 * @version 1.0
 * @since 2019-11-24
 */

package com.rest.test;

import java.util.List;
import java.util.ArrayList;


public class Cache {

    private static Cache instance;

    private List<String> parameters_n = new ArrayList<>();
    private List<Integer> parameters_k = new ArrayList<>();
    private List<Integer> results = new ArrayList<>();

    /**
     * Constructor os class Cache. It is private because this class is 
     * a singleton.
     */
    private Cache() {

    }

    /**
     * Method that initializes the singleton instance of the class.
     * @return instance of Cache.
     */
    public static Cache getInstance() {
        if (instance == null) {
            instance = new Cache();
        }
        return instance;
    }

    /**
     * Method that adds a result to the Cache System.
     * @param n      number.
     * @param k      times that the number is concatenated.
     * @param result single digit result from n*k.
     */
    public void addToCache (String n, int k, int result){

        if (results.size() >= 10){
            parameters_n.remove(0);
            parameters_k.remove(0);
            results.remove(0);
        }

        parameters_n.add(n);
        parameters_k.add(k);
        results.add(result);
    }


    /**
     * Method that checks if a given result for single digit of n*k.
     * @param  n number.
     * @param  k times that the number is concatenated.
     * @return   the result of single digit of n*k. If the result is not in cache,
     * the method will return null.
     */
    public Integer findInCache(String n, int k){

        for(int i=0; i<results.size(); i++){
            if (parameters_n.get(i).equals(n) && parameters_k.get(i) == k){
                return results.get(i);
            }
        } 

        return null;
    }

}
