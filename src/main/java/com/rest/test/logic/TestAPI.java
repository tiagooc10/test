/** 
 * TestAPI implements a api for the user CRUD and the single digit system.
 * 
 * @author Tiiago Oliveira Castro
 * @version 1.0
 * @since 2019-11-24
 */


package com.rest.test;

import java.util.List;
import java.util.ArrayList;
import java.util.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.*;

import com.rest.data.*;

import javax.inject.Singleton;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.concurrent.ThreadLocalRandom;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.concurrent.ExecutionException;
import java.io.DataOutputStream;
import java.io.IOException;



@Path("/rest")
public class TestAPI {

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/users/create")
    public Id createUser(CreateUser request){
   		System.out.println(request.toString());
		
		Id response = new Id();

		String name = request.getName();
		String email = request.getEmail();

    	UserDataAccess users = new UserDataAccess();
    	Integer id = users.createUser(name, email);
		
        if(id != null){
			response.setId(id);
		}

		users.selectAllUsers();

		return response;

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/users/read/{id}")
    public User readUser(@PathParam("id") int id) {	

    	User user = new User();

    	UserDataAccess users = new UserDataAccess();
    	user = users.readUser(id);
    	
        if(user == null)
            Response.status(Response.Status.BAD_REQUEST).entity("Not able to read User Information!").build();

		return user;    
	}


    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/users/update/{id}")
    public Response updateUser(@PathParam("id") int id, CreateUser request){
		System.out.println(request.toString());

		String name = request.getName();
		String email = request.getEmail();

    	UserDataAccess users = new UserDataAccess();
    	boolean raise = users.updateUser(id, name, email);

    	if(raise)
            return Response.status(Response.Status.OK).entity("User updated!").build();
        
        return Response.status(Response.Status.BAD_REQUEST).entity("Not able to update User Information!").build();

    }


    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/users/delete/{id}")
    public Response deleteUser(@PathParam("id") int id){

    	UserDataAccess users = new UserDataAccess();
    	boolean raise = users.deleteUser(id);
    	users.selectAllUsers();

        if(raise)
            return Response.status(Response.Status.OK).entity("User deleted!").build();

        return Response.status(Response.Status.BAD_REQUEST).entity("Not able to delete User Information!").build();
    }


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/singleDigit")
    public Result getSingleDigit(SingleDigit request){
    	System.out.println(request.toString());
		Result result = new Result();

		int id = request.getId();
		String number = request.getN();
		int k = request.getK();
    	
		Cache cache = Cache.getInstance();
		Integer result_in_cache = cache.findInCache(number,k);

        //Check if result is in Cache
		if (result_in_cache != null){
			result.setResult(result_in_cache);
			return result;
		}
        else{
            //Calculating the result because it is not in Cache
            SingleDigitCalculation single = new SingleDigitCalculation();
            int single_digit = single.getSingleDigit(id, number, k);
            result.setResult(single_digit);

        }

		return result;
  	}


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/singleDigit")
    public Result getSingleDigit(@QueryParam("number") String number, @QueryParam("k") int k){
		Result result = new Result();

		Cache cache = Cache.getInstance();
		Integer result_in_cache = cache.findInCache(number,k);

        //Check if result is in Cache
		if (result_in_cache != null){
			result.setResult(result_in_cache);
			return result;
		}
        else{
            //Calculating the result because it is not in Cache
		    SingleDigitCalculation single = new SingleDigitCalculation();
		    int single_digit = single.getSingleDigit(number,k);
		    result.setResult(single_digit);
        }


		return result;
  	}


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/results/{id}")
    public FullResults getAllResults(@PathParam("id") int id) {

    	List<FullResult> r = new ArrayList<>();

    	ResultsDataAccess results_acess = new ResultsDataAccess();
    	r = results_acess.selectAllResults(id);

    	FullResults results = new FullResults();
    	results.setResults(r);

    	return results;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/publicKey")
    public Response storePublicKey(PublicKey request){
        Result result = new Result();

        int id = request.getId();
        String public_key = request.getKey();

        UserDataAccess users = new UserDataAccess();
        boolean raise = users.updatePublicKey(id, public_key);

        if(raise)
            return Response.status(Response.Status.OK).entity("Public Key Stored!").build();

        return Response.status(Response.Status.BAD_REQUEST).entity("Error at insertion!").build();
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/encrypt")
    public Response encryptUserData(Id request){
        System.out.println(request.toString());

        int id_user = request.getId();
        UserDataAccess users = new UserDataAccess();

        String public_key = users.selectPublicKey(id_user);

        if(public_key == null){
            return Response.status(Response.Status.BAD_REQUEST).entity("No Public Key for this User!").build();
        }

        boolean raise = users.encryptUserData(id_user, public_key);

        if(raise)
            return Response.status(Response.Status.OK).entity("Data encrypted!").build();

        return Response.status(Response.Status.BAD_REQUEST).entity("Error at encrypt!").build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/decrypt")
    public Response decryptUserData(Decrypt request){
        System.out.println(request.toString());

        int id_user = request.getId();
        String private_key = request.getKey();

        UserDataAccess users = new UserDataAccess();

        if(private_key == null){
            return Response.status(Response.Status.BAD_REQUEST).entity("Invalid Private Key!").build();
        }

        boolean raise = users.decryptUserData(id_user, private_key);

        if(raise)
            return Response.status(Response.Status.OK).entity("Data decrypted!").build();

        return Response.status(Response.Status.BAD_REQUEST).entity("Error at decrypt!").build();
    }


}
