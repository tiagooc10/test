/** 
 * SingleDigitCalculation is a class responsible for calculating the single digit
 * given parameters N and K.
 * 
 * @author Tiiago Oliveira Castro
 * @version 1.0
 * @since 2019-11-24
 */


package com.rest.test;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.util.thread.*;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import java.util.concurrent.TimeUnit;
import java.net.InetSocketAddress;
import java.net.URI;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;


public class SingleDigitCalculation {


    Cache cache = Cache.getInstance();

    public static int lengthOfNumber(int number){
        return String.valueOf(number).length();
    }

    /**
    * Calculates the sum of the digits of a integer number.
    *
    * @param number
    * @return sum of digits
    */
    public static int sumOfDigits(int number){
        int sum = 0;
        while(number != 0){
            sum += number % 10;
            number = number / 10;
        }
        return sum;
    }


    /**
    * Calculates the sum of the digits of a string integer number.
    *
    * @param number
    */
    public static String sumOfDigitsFromString(String number){
        int sum = 0;

        for(int i=0; i<number.length(); i++){
            sum += Character.getNumericValue(number.charAt(i));
        }

        return String.valueOf(sum);
    }

    /**
    * Auxiliates the calculation of the single digit.
    *
    * @param number
    */
    public int calculates(String number){

        if(number.length() == 1){
            return Character.getNumericValue(number.charAt(0));
        }

        return calculates(sumOfDigitsFromString(number));

    }


    /**
    * Calculates and returns the single digit.
    *
    * @param number
    * @param k
    */
    public int getSingleDigit(int id, String number, int k){

        String p = "";

        for(int i=0; i<k; i++){
            p += number;
        }

        int single_digit = calculates(p);

        try{
            ResultsDataAccess results = new ResultsDataAccess();
            results.insertResult(id, number, k, single_digit);
            cache.addToCache(number,k,single_digit);

        }
        catch(Exception e){

        }

        return single_digit;

    }

    /**
    * Calculates and returns the single digit.
    *
    * @param number
    * @param k
    */
    public int getSingleDigit(String number, int k){

        String p = "";

        for(int i=0; i<k; i++){
            p += number;
        }

        int single_digit = calculates(p);

        try{
            cache.addToCache(number,k,single_digit);

        }
        catch(Exception e){

        }

        return single_digit;

    }


}
