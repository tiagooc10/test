package com.rest.data;

public class User{

   private String name;

   private String email;

   private int id;

   /**
    * [getName description]
    * @return [description]
    */
   public String getName(){
   	return name;
   }

   /**
    * [setName description]
    * @param name [description]
    */
   public void setName(String name){
	this.name = name;
   }

   /**
    * [getEmail description]
    * @return [description]
    */
   public String getEmail(){
	return email;
   }

   /**
    * [setEmail description]
    * @param email [description]
    */
   public void setEmail(String email){
	this.email = email;
   }

   /**
    * [getId description]
    * @return [description]
    */
   public int getId(){
      return id;
   }
   
   /**
    * [setId description]
    * @param id [description]
    */
   public void setId(int id){
   this.id = id;
   }


   @Override
   public String toString(){
	return "Login{" + "login=" + name + ", password=" + email + "}";
   }

}


