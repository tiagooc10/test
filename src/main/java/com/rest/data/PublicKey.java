package com.rest.data;

public class PublicKey{


  private int id;

  private String key;

  public int getId(){
    return id;
  }
   
  public void setId(int id){
    this.id = id;
  }

  public String getKey(){
   	return key;
  }

  public void setKey(String key){
	  this.key = key;
  }


  @Override
  public String toString(){
	  return "PublicKey{" + "key=" + key + "}";
  }
}


