package com.rest.data;

public class SingleDigit{

   private String n;
   private int id;
   private int k;


   public int getId(){
      return id;
   }

   public void setId(int id){
      this.id = id;
   }

   public String getN(){
   	return n;
   }
   
   public void setN(String n){
	this.n = n;
   }

   public int getK(){
      return k;
   }

   public void setK(int k){
   this.k = k;
   }


   @Override
   public String toString(){
	return "SingleDigit{" + "n=" + n + ", k=" + k + "}";
   }

}


