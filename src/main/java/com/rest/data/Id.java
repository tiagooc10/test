package com.rest.data;

public class Id{

   private int id;

   /**
    * [getName description]
    * @return [description]
    */
   public int getId(){
   	return id;
   }

   /**
    * [setName description]
    * @param name [description]
    */
   public void setId(int id){
	this.id = id;
   }

   @Override
   public String toString(){
	return "Id{" + "id=" + id + "}";
   }

}


