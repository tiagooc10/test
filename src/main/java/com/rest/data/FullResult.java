package com.rest.data;

public class FullResult{

    private int result;

    private String n;

    private int k;

    public int getResult(){
        return result;
    }

    public void setResult(int result){
        this.result = result;
    }

    public String getN(){
      return n;
    }
   
    public void setN(String n){
        this.n = n;
    }

    public int getK(){
      return k;
    }

    public void setK(int k){
        this.k = k;
    }

    @Override
    public String toString(){
	   return "}";
    }

}


