package com.rest.data;

import java.util.List;
import java.util.ArrayList;

public class FullResults{

   private List<FullResult> results;

   public List<FullResult> getResults(){
      return results;
   }

   public void setResults(List<FullResult> results){
      this.results = results;
   }


   @Override
   public String toString(){
	return "SingleDigit{" + "result=" + results + "}";
   }

}


