package com.rest.data;

import java.util.List;
import java.util.ArrayList;

public class Results{

   private List<Integer> results;

   public List<Integer> getResults(){
      return results;
   }

   public void setResults(List<Integer> results){
      this.results = results;
   }


   @Override
   public String toString(){
	return "SingleDigit{" + "result=" + results + "}";
   }

}


