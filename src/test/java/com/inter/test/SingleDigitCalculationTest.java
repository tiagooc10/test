package com.rest.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import static org.junit.Assert.*;   

/**
 * Unit test for Single Digit
 */

public class SingleDigitCalculationTest{

    @Test
    public void testSumOfDigitsWithZero(){

        SingleDigitCalculation s = new SingleDigitCalculation();

        assertEquals(s.sumOfDigitsFromString("10"), "1");
    }

    @Test
    public void testSumOfDigitsWithoutZero(){

        SingleDigitCalculation s = new SingleDigitCalculation();

        assertEquals(s.sumOfDigitsFromString("112345"), "16");
    }

    @Test
    public void testSumOfDigitsZero(){

        SingleDigitCalculation s = new SingleDigitCalculation();

        assertEquals(s.sumOfDigitsFromString("0"), "0");
    }

    @Test
    public void testGetSingleDigit(){

        SingleDigitCalculation s = new SingleDigitCalculation();

        assertEquals(s.getSingleDigit("9875",4), 8);
    }

    @Test
    public void testGetSingleDigit2(){

        SingleDigitCalculation s = new SingleDigitCalculation();

        assertEquals(s.getSingleDigit("9875",1), 2);
    }

}
