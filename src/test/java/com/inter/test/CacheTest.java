package com.rest.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import static org.junit.Assert.*;   

/**
 * Unit test for Cache.
 */

public class CacheTest{
 
    @Test
    public void testFindInCache(){

        Cache cache = Cache.getInstance();

        cache.addToCache("123",4,5);

	int result = cache.findInCache("123",4);
	assertEquals(result,5);
    }

    @Test
    public void testNotFindInCache(){

        Cache cache = Cache.getInstance();

        cache.addToCache("123",4,5);

	Integer result = cache.findInCache("124",4);

	assertEquals(result,null);
    }


    @Test
    public void testRemovedFromCache(){

        Cache cache = Cache.getInstance();

        cache.addToCache("1",1,1);
	    cache.addToCache("2",1,2);
	    cache.addToCache("3",1,3);
	    cache.addToCache("4",1,4);
	    cache.addToCache("5",1,5);
	    cache.addToCache("6",1,6);
	    cache.addToCache("7",1,7);
	    cache.addToCache("8",1,8);
	    cache.addToCache("9",1,9);
	    cache.addToCache("10",1,1);
	    cache.addToCache("11",1,2);

	Integer result = cache.findInCache("1",1);

	assertEquals(result,null);
    }
}
